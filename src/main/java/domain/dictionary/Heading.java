package domain.dictionary;

public enum Heading {
    SPORT, BUSINESS, GAMES, NATURE, WORK
}
