package domain.service;

import domain.model.entity.Advert;
import domain.repository.AdvertRepository;

import java.util.List;

public class AdvertService {
   private final AdvertRepository advertRepository = new AdvertRepository();

   public void save (Advert advert){
       advertRepository.save(advert);
   }

   public Advert get(Long id){
       return advertRepository.get(id);
   }

   public void update(Long id, Advert advert){
       advertRepository.update(id, advert);
   }

   public void delete(Long id){
       advertRepository.delete(id);
   }

   public List<Advert> getAll(){
       return advertRepository.getAll();
   }
}
