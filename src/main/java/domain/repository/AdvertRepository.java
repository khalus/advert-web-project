package domain.repository;

import config.Driver;
import domain.dictionary.Heading;
import domain.model.entity.Advert;
import java.io.IOException;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

public class AdvertRepository {

    private static final String TABLE_NAME = "advert";
    private static final String TABLE_ADVERT_COLUMN_ID = "id";
    private static final String TABLE_ADVERT_COLUMN_TITLE = "title" ;
    private static final String TABLE_ADVERT_COLUMN_DESCRIPTION = "description";
    private static final String TABLE_ADVERT_COLUMN_CREATING_DATE = "creating_date";
    private static final String TABLE_ADVERT_COLUMN_ENDING_DATE = "ending_date";
    private static final String TABLE_ADVERT_COLUMN_HEADING = "heading";
//    private static final String TABLE_ADVERT_COLUMN_AUTHOR = "author";
    private static final String TABLE_ADVERT_COLUMN_STATUS = "status";
    private ResultSet resultSet;

    public void save(Advert advert) {
        String sql = "INSERT INTO " + TABLE_NAME + " (" + TABLE_ADVERT_COLUMN_TITLE
                + ", " + TABLE_ADVERT_COLUMN_DESCRIPTION + ", " + TABLE_ADVERT_COLUMN_CREATING_DATE
                + ", " + TABLE_ADVERT_COLUMN_ENDING_DATE + ", " + TABLE_ADVERT_COLUMN_HEADING
                + ", " + TABLE_ADVERT_COLUMN_STATUS + ") VALUES(?,?,?,?,?,?)";
        try (PreparedStatement preparedStatement = Driver.getConnection().prepareStatement(sql)){
            preparedStatement.setString(1,advert.getTitle());
            preparedStatement.setString(2,advert.getDescription());
            preparedStatement.setDate(3,Date.valueOf(advert.getCreatingDate()));
            preparedStatement.setDate(4,Date.valueOf(advert.getEndingDate()));
            preparedStatement.setString(5,String.valueOf(advert.getHeading()));
            preparedStatement.setBoolean(6,advert.isStatus());
            preparedStatement.executeUpdate();
        } catch (SQLException | IOException ex) {
            ex.printStackTrace();
        }
    }

    public Advert get(Long id) {
        Advert advert = null;
        String sql = "SELECT " + TABLE_ADVERT_COLUMN_ID + ", " + TABLE_ADVERT_COLUMN_TITLE
                + ", " + TABLE_ADVERT_COLUMN_DESCRIPTION + ", " + TABLE_ADVERT_COLUMN_CREATING_DATE
                + ", " + TABLE_ADVERT_COLUMN_ENDING_DATE + ", " + TABLE_ADVERT_COLUMN_HEADING + ", "
                + "," +  TABLE_ADVERT_COLUMN_STATUS
                + " FROM " + TABLE_NAME + " WHERE " + TABLE_ADVERT_COLUMN_ID + " = ?";
        try( PreparedStatement preparedStatement = Driver.getConnection().prepareStatement(sql)){
            preparedStatement.setLong(1, id);
            resultSet = preparedStatement.executeQuery();
            while(resultSet.next()){
                Long advertId = resultSet.getLong(1);
                String title = resultSet.getString(2);
                String description = resultSet.getString(3);
                LocalDate creatingDate = resultSet.getDate(4).toLocalDate();
                LocalDate endingDate = resultSet.getDate(5).toLocalDate();
                Heading heading = (Heading) resultSet.getObject(6);
                boolean status = resultSet.getBoolean(7);
                advert = new Advert(advertId,title,description,creatingDate,endingDate,heading,status);
            }
            return advert;
        } catch (SQLException | IOException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public void update(Long id, Advert advert) {
        String sql = "UPDATE " + TABLE_NAME + " SET " + TABLE_ADVERT_COLUMN_TITLE + "=?, "
                + TABLE_ADVERT_COLUMN_DESCRIPTION + "=?, " + TABLE_ADVERT_COLUMN_ENDING_DATE + "=?, "
                + TABLE_ADVERT_COLUMN_HEADING + "=?," + TABLE_ADVERT_COLUMN_STATUS + "=?" + " WHERE " + TABLE_ADVERT_COLUMN_ID + "=?";
        try(PreparedStatement preparedStatement = Driver.getConnection().prepareStatement(sql)) {
            preparedStatement.setString(1, advert.getTitle());
            preparedStatement.setString(2, advert.getDescription());
            preparedStatement.setDate(3, Date.valueOf(advert.getEndingDate()));
            preparedStatement.setString(4,String.valueOf(advert.getHeading()));
            preparedStatement.setBoolean(5, advert.isStatus());
            preparedStatement.setLong(6,id);
            preparedStatement.executeUpdate();
        } catch (SQLException | IOException ex) {
            System.out.println(ex);
        }
    }


    public void delete(Long id) {
        String sql = "DELETE FROM " + TABLE_NAME + " WHERE " + TABLE_ADVERT_COLUMN_ID + " =  ?";
        try(PreparedStatement preparedStatement = Driver.getConnection().prepareStatement(sql)) {
            preparedStatement.setLong(1, id);
            preparedStatement.executeUpdate();
        } catch (SQLException | IOException ex) {
            System.out.println(ex);
        }
    }

    public List<Advert> getAll() {
        List<Advert> adverts = new LinkedList<>();
        String sql = "SELECT " + TABLE_ADVERT_COLUMN_ID + ", " + TABLE_ADVERT_COLUMN_TITLE
                + ", " + TABLE_ADVERT_COLUMN_DESCRIPTION + ", " + TABLE_ADVERT_COLUMN_CREATING_DATE
                + ", " + TABLE_ADVERT_COLUMN_ENDING_DATE + ", " + TABLE_ADVERT_COLUMN_HEADING + ", "
                + "," +  TABLE_ADVERT_COLUMN_STATUS
                + " FROM " + TABLE_NAME;
        try(PreparedStatement preparedStatement = Driver.getConnection().prepareStatement(sql)) {
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                Long advertId = resultSet.getLong(1);
                String title = resultSet.getString(2);
                String description = resultSet.getString(3);
                LocalDate creatingDate = resultSet.getDate(4).toLocalDate();
                LocalDate endingDate = resultSet.getDate(5).toLocalDate();
                Heading heading = (Heading) resultSet.getObject(6);
                boolean status = resultSet.getBoolean(7);
                adverts.add(new Advert(advertId,title,description,creatingDate,endingDate,heading,status));
            }
        } catch (SQLException | IOException ex) {
            System.out.println(ex);
        }
        return adverts;
    }
}
