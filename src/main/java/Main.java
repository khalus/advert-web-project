
import domain.dictionary.Heading;
import domain.model.entity.Advert;
import domain.service.AdvertService;

import java.time.LocalDate;

public class Main {
    public static void main(String[] args) {
        AdvertService advertService = new AdvertService();
        Advert advert = new Advert(3L,"Title","Description", LocalDate.now(),LocalDate.of(2021,11,10), Heading.BUSINESS, true);
        advertService.save(advert);
        advertService.get(3L);
    }

}
